variable "environment" {
  default = "test"
}

variable "region" {
  default = "us-east-1"
}

variable "default_tags" {
  default     = {}
  description = "default tags to resources"
}
