provider "aws" {
  region = var.region
  version = "~> 3.40"
}

module "alb" {
  source       = "./modules/alb"
}
